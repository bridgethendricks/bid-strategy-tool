# server_login.R

# Login
output$username.login <- renderUI({
  if(is.null(SysResponse())){
    list(textInput("username", "UserName"),
         passwordInput("password", "Password"),
         actionButton("action", "Engage!"))
  } else {
    if(SysResponse()$HasAccessToAllAgencies == FALSE){
      list(textInput("username", "UserName"),
           passwordInput("password", "Password"),
           actionButton("action", "Engage!"))
    }
  }
})

# Return the requested dataset
SysResponse <- reactive({
  if(is.null(input$action)){return(NULL)}
  if(input$action == 0){return(NULL)}
  sys.call <- paste("curl -F \"type=text/html;\" --header \"UserName:",
                    input$username,
                    "\" --header \"Password:",
                    input$password,
                    "\" http://vivaodpws1-ue1t/GetPermissionData/",
                    sep = "")
  sys.response <- fromJSON(system(sys.call, intern = T))$Result
  sys.response
})
