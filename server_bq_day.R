
BQ.Day <- reactive({
  if(is.null(kpi.name())){ return(NULL) }
  
  progress <- Progress$new()
  progress$set(message = "Querying Data...", value = 0)
  on.exit(progress$close())
  updateProgress <- function(value = NULL, detail = NULL) {
    if (is.null(value)) {
      value <- progress$getValue()
      value <- value + (progress$getMax() - value) / 3
    }
    progress$set(value = value, detail = detail)
  }
  
  tbl.nm <- toString(BQ()$tbl.nm) 
  updateProgress()
  
  cpuu <- cpuu.values()
  cpc <- cpc.values()
  cpa <- cpa.values()
  
  kpi.name <- kpi.name()
  if(kpi.name == "unique users"){
    dat <- cpuu
  } else {
    if(kpi.name == "click"){
       dat <- cpc
    } else {
      dat <- cpa
    }
  }
  updateProgress()
  dat
})
