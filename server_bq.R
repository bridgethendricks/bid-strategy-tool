##### BigQuery #####

BQ <- eventReactive(input$bq.action, {
  if(is.null(input$bq.action)){ return(NULL) }
  if(input$bq.action == 0){ return(NULL) }
  if(input$Agency == ""){ return(NULL) }
  if(input$Advertiser == ""){ return(NULL) }
  if(input$Lineitem == ""){ return(NULL) }
  
  isolate({# Status Bar Logic -- Needed when testing large functions to make sure
  # that the query is still running
  progress <- shiny::Progress$new()
  progress$set(message = "Initializing Data...", value = 0)
  on.exit(progress$close())
  updateProgress <- function(value = NULL, detail = NULL) {
    if (is.null(value)) {
      value <- progress$getValue()
      value <- value + (progress$getMax() - value) / 2
    }
    progress$set(value = value, detail = detail)
  }
  
  agency <- input$Agency # agency <- 146935
  advertiser <- input$Advertiser # advertiser <- 577363
  li_id <- input$Lineitem # li_id <- 2756215
  tbl.nm <- paste("DBM", agency, advertiser, sep = "_") # 
    
  try(delete_table(project, "AnalyticsSandbox", tbl.nm))
  updateProgress()

  sql.compare <- paste0(
              "SELECT DATE(USEC_TO_TIMESTAMP(INTEGER(event_time))) AS dates, 
              AVG(total_media_cost_usd_nanos) AS total, 
              AVG(bid_price_usd_nanos) AS bid, 
              COUNT(bid_price_usd_nanos) AS imps 
              FROM [dbmLog.", tbl.nm, "_view]
              WHERE INTEGER(line_item_id) = ", li_id, " 
              GROUP BY dates ORDER BY dates")
  time.Table <- query_exec(sql.compare, project = project, max_pages = Inf)
  time.Table$cpm <- time.Table$total / time.Table$imps
 
  list(time.Table = time.Table, tbl.nm = tbl.nm, li_id = li_id)})
})
